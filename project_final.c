/*-------------Description--------------*/
/*this program is used to compute the forward problem in the boundary integral formulation Eq.(12) in the paper.
The output of the file is the error at the marker point(s). The program will run for either one marker point or
various marker points depending on which portion of the code is commented out. */

static char help[] = "This program computes the forward problem";
#include <petscvec.h>
#include <math.h>
#include <mpi.h>
#include<stdio.h>
#include <petscmat.h>
#include <petscksp.h>

#undef __FUNCT__
#define __FUNCT__ "main"


extern PetscErrorCode Outer(PetscScalar*,PetscScalar*,PetscScalar*);
extern PetscErrorCode Identity(PetscScalar*);
extern PetscErrorCode Green(PetscScalar*,PetscScalar*,PetscScalar,PetscScalar*);
extern PetscErrorCode Sign(PetscScalar,PetscScalar);

PetscScalar func(PetscScalar a)
{
  if(a>0.0){
	return fabs(a)+1;
	}
  else{
	return -(fabs(a)+1);
	}
}

int main(int argc,char **argv)

{

\*Here n and np are the mesh sizes in the t and phi directions respectively, 
and m is the number of marker points in each direction.*\
  PetscErrorCode ierr;
  int		 rank,size;
  PetscInt       n=32,np=32,maxind,start,end,l,phiind,tind,d,i,j,m=16;
  PetscScalar    h = 1.0/n, hp = 2*M_PI/np,t0,phi0,theta0,rho,s;
  PetscScalar	 *Vyarray,*Vzarray,*Vxarray,phip,tp,*x0,mesh[3],*p,theta,*hh,coef,*eye,one =1.0,lambda2 =1.0,*f11;
  PetscScalar	 *xunarray,dot,*g,*i2,xvel,diffnorm,*sumarray1,*sumarray2,yvel,zvel,*Velarray,error;
  PetscScalar	 *xunr1,*xunr2;
  Vec            marker,Vx,Vy,Vz,x,absx,P,y,xun,f1,f2,q,v,I1,I2,diff,Vnum,e;
  Mat		 H,Eye,F11,G,I22;

  PetscInitialize(&argc,&argv,(char*)0,help);


  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);

/*-----------------------Defining marker point----------------*/ 
/*--------------------comment this block out for only one marker point----*/
/*for(i =0;i<m+1;i++){
for(j =0;j<m+1;j++){
  t0 = i*h;
  phi0 = j*hp;*/
/*-----------------------------------------------------------------------*/
/*-------------------comment this block out for 16 by 16 marker points -----*/
  t0 = 8*h;
  phi0 = 10*h;
/*--------------------------------------------------------------------------*/
  theta0 = M_PI*(t0-(sin(2*M_PI*t0))/(2*M_PI));
  PetscMalloc1(3,&x0);
  x0[0] = sin(theta0)*cos(phi0);
  x0[1] = sin(theta0)*sin(phi0);
  x0[2] = cos(theta0);

  VecCreateSeqWithArray(MPI_COMM_SELF,1,3,x0,&marker);
  VecAssemblyBegin(marker);
  VecAssemblyEnd(marker);
/*------------------------Defining true velocity----------------*/
  VecCreate(PETSC_COMM_WORLD,&v);
  VecSetSizes(v,3,3);
  VecSetFromOptions(v);
  VecSet(v,one);
/*-------------------getting rho-------------------------------------*/
  VecDuplicate(marker,&absx);
  VecCopy(marker,absx);
  VecAbs(absx);
  VecMax(absx,&maxind,&rho);
  VecDestroy(&absx);

 
/*-----------------------Defining Householder matrix-------------------------*/
  PetscMalloc1(3,&p);
  PetscMalloc1(9,&hh);
  MatCreate(MPI_COMM_SELF,&H);
  MatSetSizes(H,3,3,3,3);
  MatSetType(H,MATSEQDENSE);
  PetscMalloc1(9,&eye);
  Identity(eye);
  MatCreate(MPI_COMM_SELF,&Eye);
  MatSetSizes(Eye,3,3,3,3);
  MatSetType(Eye,MATSEQDENSE);
  MatSeqDenseSetPreallocation(Eye,eye);
  PetscMallocDump(&eye);
  MatSeqDenseSetLDA(Eye,3);
  MatAssemblyBegin(Eye,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(Eye,MAT_FINAL_ASSEMBLY);
/*-----------------------Cases for Householder ---------------------*/
  if(rho == x0[2]){
  p[0] = x0[0];
  p[1] = x0[1];
  p[2] = func(x0[2]);

  VecCreateSeqWithArray(MPI_COMM_SELF,1,3,p,&P);
  VecAssemblyBegin(P);
  VecAssemblyEnd(P);

  coef = 1.0/ sqrt(2.0+ 2.0*fabs(x0[2]));
  VecScale(P,coef);

  Outer(p,p,hh);       
  MatSeqDenseSetPreallocation(H,hh);
  MatSeqDenseSetLDA(H,3);
  MatAssemblyBegin(H,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(H,MAT_FINAL_ASSEMBLY);  
  MatAYPX(H,-2.0,Eye,SAME_NONZERO_PATTERN);
	}
  else if(rho == x0[1]){
  p[0] = x0[2];
  p[1] = x0[0];
  p[2] = func(x0[1]);  
  VecCreateSeqWithArray(MPI_COMM_SELF,1,3,p,&P);
  VecAssemblyBegin(P);
  VecAssemblyEnd(P);

  coef = 1.0/ sqrt(2.0+ 2.0*fabs(x0[1]));
  VecScale(P,coef);

  Outer(p,p,hh);
  MatSeqDenseSetPreallocation(H,hh);
  MatSeqDenseSetLDA(H,3);
  MatAssemblyBegin(H,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(H,MAT_FINAL_ASSEMBLY);
  MatAYPX(H,-2.0,Eye,SAME_NONZERO_PATTERN);
	}
  else{
  p[0] = x0[1];
  p[1] = x0[2];
  p[2] = func(x0[0]);

  VecCreateSeqWithArray(MPI_COMM_SELF,1,3,p,&P);
  VecAssemblyBegin(P);
  VecAssemblyEnd(P);

  coef = 1.0/ sqrt(2.0+ 2.0*fabs(x0[0]));
  VecScale(P,coef);

  Outer(p,p,hh);
  MatSeqDenseSetPreallocation(H,hh);
  MatSeqDenseSetLDA(H,3);
  MatAssemblyBegin(H,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(H,MAT_FINAL_ASSEMBLY);
  MatAYPX(H,-2.0,Eye,SAME_NONZERO_PATTERN);
	}

/*--------------------obtaining velocities------------------------------*/
  VecCreate(PETSC_COMM_WORLD,&Vx);
  VecSetSizes(Vx,PETSC_DECIDE,(n+1)*(np+1));
  VecSetFromOptions(Vx);
  VecZeroEntries(Vx);
  
  VecCreate(PETSC_COMM_WORLD,&Vy);
  VecSetSizes(Vy,PETSC_DECIDE,(n+1)*(np+1));
  VecSetFromOptions(Vy);
  VecZeroEntries(Vy);

  VecCreate(PETSC_COMM_WORLD,&Vz);
  VecSetSizes(Vz,PETSC_DECIDE,(n+1)*(np+1));
  VecSetFromOptions(Vz);
  VecZeroEntries(Vz);
  
  VecGetOwnershipRange(Vx,&start,&end);
  VecGetArray(Vx,&Vxarray);
  VecGetArray(Vy,&Vyarray);
  VecGetArray(Vz,&Vzarray);

  d = 0;
  for(l = start;l<end;l++){
  phiind = l-floor(l/(np+1))*(np+1);
  tind = floor(l/(np+1));
  phip = phiind*hp;
  tp = tind*h;
  theta = M_PI*(tp-(sin(2*M_PI*tp))/(2*M_PI));
/*  PetscPrintf(PETSC_COMM_SELF,"t is %g and phi is %g\n",(double)tp, (double)phip);*/

  mesh[0] = sin(theta)*cos(phip);
  mesh[1] = sin(theta)*sin(phip);
  mesh[2] = cos(theta);

  VecCreateSeqWithArray(MPI_COMM_SELF,1,3,mesh,&x);
  VecAssemblyBegin(x);
  VecAssemblyEnd(x);
  
  VecCreate(PETSC_COMM_SELF,&y);
  VecSetSizes(y,3,3);
  VecSetFromOptions(y);
  VecZeroEntries(y);
  MatMult(H,x,y);

  PetscMalloc1(3,&xunr1);
  VecGetArray(y,&xunr1);
  PetscMalloc1(3,&xunr2);
  if(rho == x0[2]){
  xunr2[0] = xunr1[0];
  xunr2[1] = xunr1[1];
  xunr2[2] = xunr1[2];
	}
  else if(rho == x0[1]){
  xunr2[0] = xunr1[1];
  xunr2[1] = xunr1[2];
  xunr2[2] = xunr1[0];
	}
  else{
  xunr2[0] = xunr1[2];
  xunr2[1] = xunr1[0];
  xunr2[2] = xunr1[1];
	}	
  VecRestoreArray(y,&xunr1);
  VecCreateSeqWithArray(MPI_COMM_SELF,1,3,xunr2,&xun);
  VecAssemblyBegin(xun);
  VecAssemblyEnd(xun);

/*  VecCopy(y,xun); */

  VecCreate(PETSC_COMM_SELF,&f1);
  VecSetSizes(f1,3,3);
  VecSetFromOptions(f1);
  VecZeroEntries(f1);

  VecCreate(PETSC_COMM_SELF,&f2); 
  VecSetSizes(f2,3,3); 
  VecSetFromOptions(f2);
  VecCopy(xun,f2); 

  VecCreate(PETSC_COMM_SELF,&q); 
  VecSetSizes(q,3,3); 
  VecSetFromOptions(q);  
  VecZeroEntries(q); 

  MatCreate(PETSC_COMM_SELF,&F11);
  MatSetSizes(F11,3,3,3,3);
  MatSetType(F11,MATSEQDENSE);
  PetscMalloc1(9,&f11);
  VecGetArray(xun,&xunarray);
  Outer(xunarray,xunarray,f11);
  VecRestoreArray(xun,&xunarray);
  MatSeqDenseSetPreallocation(F11,f11);
  MatAssemblyBegin(F11,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(F11,MAT_FINAL_ASSEMBLY);
  MatScale(F11,lambda2/3.0);
  MatAXPY(F11,1.0+sqrt(lambda2),Eye,SAME_NONZERO_PATTERN);
  MatMult(F11,v,f1);
  VecScale(f1,-1.5);
 
  VecDot(xun,v,&dot);
  VecScale(f2,lambda2*dot);
  VecWAXPY(q,-1.0,f2,f1);

  /*----------------the integrals-------------*/
  MatCreate(PETSC_COMM_SELF,&I22);
  MatSetSizes(I22,3,3,3,3);
  MatSetType(I22,MATSEQDENSE);
  PetscMalloc1(9,&i2);
  VecGetArray(xun,&xunarray);
  Outer(x0,xunarray,i2);
  VecRestoreArray(xun,&xunarray);
  MatSeqDenseSetPreallocation(I22,i2);
  MatAssemblyBegin(I22,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(I22,MAT_FINAL_ASSEMBLY);

  VecCreate(PETSC_COMM_SELF,&I2);
  VecSetSizes(I2,3,3);
  VecSetFromOptions(I2);
  VecZeroEntries(I2);
  MatMult(I22,q,I2);
  VecScale(I2,sin(theta)*M_PI*(1.0-cos(2*M_PI*tp)));

  PetscMalloc1(3,&sumarray2);
  VecGetArray(I2,&sumarray2);
  Vxarray[d] = sumarray2[0];
  Vyarray[d] = sumarray2[1];
  Vzarray[d] = sumarray2[2];

  if(l-floor(l/(np+1))*(np+1)==0){
        Vxarray[d]=0.0;        
        Vyarray[d]=0.0;                
        Vzarray[d]=0.0;                
        }
  VecRestoreArray(I2,&sumarray2);

  VecCreate(PETSC_COMM_SELF,&diff);
  VecSetSizes(diff,3,3);
  VecSetFromOptions(diff);
  VecZeroEntries(diff);  
  VecWAXPY(diff,-1.0,xun,marker);
  VecNorm(diff,NORM_2,&diffnorm);

  if(diffnorm<1.0e-12){
	continue;
	}
  VecDestroy(&diff);

/*-------------Making the Greens function-------*/
  MatCreate(PETSC_COMM_SELF,&G);
  MatSetSizes(G,3,3,3,3);
  MatSetType(G,MATSEQDENSE);
  PetscMalloc1(9,&g);
  VecGetArray(xun,&xunarray);
  Green(xunarray,x0,lambda2,g);
  VecRestoreArray(xun,&xunarray);
  MatSeqDenseSetPreallocation(G,g);
  MatAssemblyBegin(G,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(G,MAT_FINAL_ASSEMBLY);
  VecDestroy(&xun);
 
  VecCreate(PETSC_COMM_SELF,&I1);
  VecSetSizes(I1,3,3);
  VecSetFromOptions(I1);
  VecZeroEntries(I1);
  MatMult(G,q,I1);
  VecScale(I1,sin(theta)*M_PI*(1.0-cos(2*M_PI*tp)));

  PetscMalloc1(3,&sumarray1);
  VecGetArray(I1,&sumarray1);

  Vxarray[d] = Vxarray[d]+sumarray1[0];
  Vyarray[d] = Vyarray[d]+sumarray1[1];
  Vzarray[d] = Vzarray[d]+sumarray1[2];



  if(l-floor(l/(np+1))*(np+1)==0){
        Vxarray[d]=0.0;        
        Vyarray[d]=0.0;        
        Vzarray[d]=0.0;        
        }
  VecRestoreArray(I1,&sumarray1);
  VecDestroy(&y);  
  VecDestroy(&x);
  d++;
  }

  VecRestoreArray(Vx,&Vxarray);
  VecRestoreArray(Vy,&Vyarray);
  VecRestoreArray(Vz,&Vzarray);

  VecScale(Vx,-h*hp/(8.0*M_PI));
  VecSum(Vx,&xvel);
/*  PetscPrintf(PETSC_COMM_WORLD,"the xvel is %g\n",(double)xvel); */

  VecScale(Vy,-h*hp/(8.0*M_PI));
  VecSum(Vy,&yvel);
/*  PetscPrintf(PETSC_COMM_WORLD,"the yvel is %g\n",(double)yvel);*/

  VecScale(Vz,-h*hp/(8.0*M_PI));
  VecSum(Vz,&zvel);
/*  PetscPrintf(PETSC_COMM_WORLD,"the zvel is %g\n",(double)zvel);*/

  PetscMalloc1(3,&Velarray);  
  Velarray[0] = xvel;
  Velarray[1] = yvel;
  Velarray[2] = zvel;

  VecCreateSeqWithArray(MPI_COMM_SELF,1,3,Velarray,&Vnum);
  VecAssemblyBegin(Vnum);
  VecAssemblyEnd(Vnum);

  VecCreate(PETSC_COMM_SELF,&e);
  VecSetSizes(e,3,3);
  VecSetFromOptions(e);
  VecZeroEntries(e);
  VecWAXPY(e,-1.0,Vnum,v);
  VecNorm(e,NORM_2,&error);
  PetscPrintf(PETSC_COMM_WORLD,"the error is %g\n",(double)error);*/

  VecDestroy(&Vx);
  VecDestroy(&Vy);
  VecDestroy(&Vz);
  VecDestroy(&Vnum);
  VecDestroy(&e);
  VecDestroy(&v);
  VecDestroy(&marker);
/*}
}*/
  ierr = PetscFinalize();
  return 0;
}

/*-----------------------------------*/

PetscErrorCode Outer(PetscScalar *p,PetscScalar *w,PetscScalar *hh)
{
  PetscInt i,j;
  for(i=0;i<3;i++){
	for(j=0;j<3;j++){
	hh[j+3*i] = p[j]*w[i];
	}
  }
  return(0);
}

/*----------------------------*/

PetscErrorCode Identity(PetscScalar *eye)
{
  PetscInt i,j;
  for(i=0;i<3;i++){
        for(j=0;j<3;j++){
	if(i==j){
	eye[i+3*j]=1.0;
	}	
	else{
	eye[i+3*j]=0.0;
	}
        }
  }
  return(0);
}

/*--------------Green's function-----------------*/
PetscErrorCode Green(PetscScalar *xunarray, PetscScalar *x0, PetscScalar lambda2,PetscScalar *g)
{
  PetscScalar	Xarray[3],r,X,A,B;
  PetscInt	i,j;

  for(i=0;i<3;i++){
  	Xarray[i] = xunarray[i] - x0[i];
  }
  r = sqrt(Xarray[0]*Xarray[0] + Xarray[1]*Xarray[1] + Xarray[2]*Xarray[2]);
  X = sqrt(lambda2)*r;
  A = 2*exp(-X)*(1+1/X+1/(X*X))-2/(X*X);
  B = -2*exp(-X)*(1+3/X+3/(X*X)) + 6/(X*X);

  for(i=0;i<3;i++){
        for(j=0;j<3;j++){
        if(i==j){
        g[i+3*j] = A/r + B/(r*r*r)*Xarray[i]*Xarray[i];
        }
        else{
        g[i+3*j] = B/(r*r*r)*Xarray[i]*Xarray[j];
        }
        }
  }
  return(0);
}

